# Advent of Code 🎄

**Author:** William Johansson  
**GitLab:** @ancientrubberduck

My solutions for [Advent of Code](https://adventofcode.com/) challenge.

## What is Advent of Code?

[Advent of Code](http://adventofcode.com) is an online event created by [Eric Wastl](https://twitter.com/ericwastl). Each year, starting on Dec 1st, an advent calendar of small programming puzzles are unlocked once a day at midnight (EST/UTC-5). Developers of all skill sets are encouraged to solve them in any programming language they choose!

## 2020 Progress

| Day  | Part One | Part Two |
|---|:---:|:---:|
| ✔ [Day 1: Report Repair](https://adventofcode.com/2020/day/1) ([Solution](https://gitlab.com/ancientrubberduck/Advent-of-Code/-/tree/main/2020/day1))|⭐|⭐|
| ✔ [Day 2: Password Philosophy](https://adventofcode.com/2020/day/2) ([Solution](https://gitlab.com/ancientrubberduck/Advent-of-Code/-/tree/main/2020/day2))|⭐|⭐|
| ✔ [Day 3: Toboggan Trajectory](https://adventofcode.com/2020/day/3) ([Solution](https://gitlab.com/ancientrubberduck/Advent-of-Code/-/tree/main/2020/day3))|⭐|⭐|
| ✔ [Day 4: Passport Processing](https://adventofcode.com/2020/day/4) ([Solution](https://gitlab.com/ancientrubberduck/Advent-of-Code/-/tree/main/2020/day4))|⭐|⭐|
| ✔ [Day 5: Binary Boarding](https://adventofcode.com/2020/day/5) ([Solution](https://gitlab.com/ancientrubberduck/Advent-of-Code/-/tree/main/2020/day5))|⭐|⭐|
| ✔ [Day 6: Custom Customs](https://adventofcode.com/2020/day/6) ([Solution](https://gitlab.com/ancientrubberduck/Advent-of-Code/-/tree/main/2020/day6))|⭐|⭐|
