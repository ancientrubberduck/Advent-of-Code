<#
    --- Day 3: Toboggan Trajectory ---    
    https://adventofcode.com/2020/day/3
#>

#------------- Part 1 -------------#
$Map = Get-Content '.\input.txt'

$x = 3
$position = 0
$MapWith = $map[0].Length -1
$TreeCount = 0

# Move down each line in the map
foreach ($Line in $Map) {
    # Skip the first line
    if ($Map.IndexOf($Line) -eq 0) {
        $positionValue = $Line[$position]
    }

    else {
        # Move 3 positions to the right
        $position += $x

        # Check if position is out of the map
        if ($position -gt $MapWith) {
            # loop the position back to the start of the line
            $position = $position - ($MapWith + 1)
        }

        # Collect the value of the current position
        $positionValue = $Line[$position]


        # Check if the position is on a tree
        switch ($positionValue) {
            '.' {  }
            '#' { $TreeCount++ }
        }
    }
}

Write-Host '--- Day 3: Toboggan Trajectory ---'
Write-Host "Found: $($TreeCount) trees" -ForegroundColor Green
