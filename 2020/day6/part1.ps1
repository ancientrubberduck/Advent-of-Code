[CmdletBinding()]
param ()
<#
    --- Day 6: Custom Customs ---    
    https://adventofcode.com/2020/day/6
#>

#------------- Part 1 -------------#
$PuzzleInput = Get-Content '.\input.txt'

$nl = [System.Environment]::NewLine
$PuzzleInput = (Get-content '.\input.txt' -Raw) -split ("$nl$nl") -replace " ", ""  # Split into groups
$PuzzleInput = $PuzzleInput -replace $nl,""     # Format data to be one string

$Count = 0

foreach ($String in $PuzzleInput) {

    # Convert the sring to a Char Array
    $charArr = $String.ToCharArray()

    # Select only the uniqe chars
    $uniqe = $charArr | Select-Object -Unique

    # Add to the count
    $Count += $uniqe.count
}

return $Count
