[CmdletBinding()]
param ()
<#
    --- Day 6: Custom Customs ---    
    https://adventofcode.com/2020/day/6
#>

#------------- Part 2 -------------#
$nl = [System.Environment]::NewLine
$PuzzleInput = Get-Content '.\input.txt' -Delimiter "$nl$nl"

#$nl = [System.Environment]::NewLine
#$PuzzleInput = (Get-content '.\input.txt' -Raw) -split ("$nl$nl") -replace " ", ""  # Split into groups
#$PuzzleInput = $PuzzleInput -replace $nl,""     # Format data to be one string

$Count = @()

foreach ($String in $PuzzleInput) {

    # Count the number of the persons in the group
    $PersonCount = ($String -split $nl).count

    # Convert the sring to a Char Array
    $charArr = $String.ToCharArray()

    # Select only the uniqe chars
    $NonUniqe = ($charArr | Group-Object | Where-Object {$_.Count -ge $PersonCount})

    # Add to the count
    $Count += $NonUniqe
}

return ($Count | Measure-Object).Count
