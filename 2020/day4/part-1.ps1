<#
    --- Day 4: Passport Processing ---  
    https://adventofcode.com/2020/day/4
#>
#------------- Part 1 -------------#

$nl = [System.Environment]::NewLine
$PuzzleInput = (Get-content '.\input.txt' -Raw) -split ("$nl$nl") -replace " ", "`n"

$ValidCount = 0
$NotValidCount = 0

# Required fields
$requiredFields = @(
    'byr'   # Birth Year
    'iyr'   # Issue Year
    'eyr'   # Expiration Year
    'hgt'   # Height
    'hcl'   # Hair Color
    'ecl'   # Eye Color
    'pid'   # Passport ID
    #'cid'   # Country ID
)

$IgnoredFields = @(
    
)


foreach ($line in $PuzzleInput) {
    $data = $line | ConvertFrom-StringData -Delimiter ":"

    $Valid = $true

    foreach ($field in $requiredFields) {
    
        if (! ($data.ContainsKey($field)) ) {
            $Valid = $false
        }
    }

    if ($Valid -eq $true) {
        $ValidCount++
    }
    else {
        $NotValidCount++
    }
}

Write-Host "==== Passport Validation ===="
Write-Host "Valid: $($ValidCount)"
Write-Host "Not Valid: $($NotValidCount)"
