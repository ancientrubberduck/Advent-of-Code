<#
    --- Day 4: Passport Processing ---  
    https://adventofcode.com/2020/day/4
#>
#------------- Part 2 -------------#

$nl = [System.Environment]::NewLine
$PuzzleInput = (Get-content '.\input.txt' -Raw) -split ("$nl$nl") -replace " ", "`n"
$PuzzleInput = $PuzzleInput | ConvertFrom-StringData -Delimiter ':' # Convert to a hashtable 

# Part 2
function Test-Passport {
    [CmdletBinding()]
    param (
        [ValidateRange(1920, 2002)]
        $byr,
        [ValidateRange(2010, 2020)]
        $iyr,
        [ValidateRange(2020, 2030)]
        $eyr,
        [ValidatePattern("^\d+(cm|in)")]
        [ValidateScript( {
                $num = $_ -replace "\D"
                # IF CM 150-193
                if ($_ -match "cm") {
                    if ($num -ge 150 -and $num -le 193) { $true }
                }
                # IF IN 59-76
                else {
                    if ($num -ge 59 -and $num -le 76) { $true }
                }
            })]
        $hgt,
        [ValidatePattern("^#[0-9a-f]{6}$")]
        $hcl,
        [ValidateSet("amb", "blu", "brn", "gry", "grn", "hzl", "oth")]
        $ecl,
        [ValidatePattern("^[0-9]{9}$")]
        $pid_,
        $cid
    )

    if ($byr -and $iyr -and $eyr -and $hgt -and $hcl -and $ecl -and $pid_) {
        return $true
    }
    else {
        return $false
    }
}

$ValidPassports = foreach ( $Passport in $PuzzleInput ) {
    try { 
        Test-Passport @Passport -ErrorAction Stop
    }
    catch {}
}


Write-Host "==== Passport Validation ===="
Write-Host "Valid: $(($ValidPassports | Where-Object {$_ -eq $true} ).count)"
Write-Host "Not Valid: $(($ValidPassports | Where-Object {$_ -eq $false} ).count)"

