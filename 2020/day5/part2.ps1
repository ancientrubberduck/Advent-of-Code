[CmdletBinding()]
param ()
<#
    --- Day 5: Binary Boarding ---    
    https://adventofcode.com/2020/day/5
#>

#------------- Part 2 -------------#
<#
    --- Test input ---
    BFFFBBFRRR: row 70, column 7, seat ID 567.
    FFFBBBFRRR: row 14, column 7, seat ID 119.
    BBFFBBFRLL: row 102, column 4, seat ID 820.
#>
$PuzzleInput = Get-Content '.\input.txt'
#$TestInput = @('BFFFBBFRRR','FFFBBBFRRR','BBFFBBFRLL')

Function Get-BoardingPassDetails {
    [CmdletBinding()]
    param ( 
        [string]$BoardingPass
    )

    $rows = 0..127
    $columns = 0..7

    $CharArr = $BoardingPass.ToCharArray()

    foreach ($char in $CharArr) {
        switch ($char) {
            'F' {
                # Lower half of the array
                $rows = $rows[0..([math]::truncate(($rows.GetUpperBound(0)/2)))]
            }
            'B' {
                # Upper half of the array
                # Add 0.005 before [math]::round to prevent PowerShell rounding 0.5 to 0 and not 1
                $rows = $rows[([math]::round(($rows.GetUpperBound(0)/2) + 0.005))..($rows.GetUpperBound(0))]
            }
            'L'{
                # Lower half of the array
                $columns = $columns[0..([math]::truncate(($columns.GetUpperBound(0)/2)))]
            }
            'R' {
                # Upper half of the array
                # Add 0.005 before [math]::round to prevent PowerShell rounding 0.5 to 0 and not 1
                $columns = $columns[([math]::round(($columns.GetUpperBound(0)/2) + 0.005))..($columns.GetUpperBound(0))]
            }
        }
    }
    Write-Verbose '==============================='
    Write-Verbose "Boarding Pass: $($BoardingPass)"
    Write-Verbose "Row: $($rows)"
    Write-Verbose "Seat: $($columns)"
    Write-Verbose "Seat ID: $(($rows[0] * 8) + $columns[0])"
    Write-Verbose '==============================='

    # Return the seat ID
    return ($rows[0] * 8) + $columns[0]
}

$SeatIDs = @()

foreach ($BoardingPass in $PuzzleInput) {

    $SeatIDs += Get-BoardingPassDetails -BoardingPass $BoardingPass
}



# Result
return ($SeatIDs | Sort-Object)[0]..($SeatIDs | Sort-Object)[-1] | Where-Object {$_ -notin $SeatIDs}